# README #

### What is this repository for? ###

This public repo will download properties.dtd from oracle.com then upload to repository's download page; This is to allow Pipelines build to download without being rate limited by oracle.com.

:information_source: A weekly schedule build will run to ensure the properties.dtd is updated. 

File source: http://www.oracle.com/webfolder/technetwork/jsc/dtd/properties.dtd

### How do I get set up? ###

* Navigate to this repository's **Downloads** page (Left Navigation bar)
* Right click on *'properties.dtd'* and copy the URL
* Include the URL to your build